import Vue from "vue";
import App from "./components/App/App.vue";
import VueRouter from "vue-router";
import ListBeers from "./components/Beer/ListBeers/ListBeers.vue";
import ShowBar from "./components/Bar/ShowBar/ShowBar.vue";
import UpdateBar from "./components/Bar/UpdateBar/UpdateBar.vue";
import AddBar from "./components/Bar/AddBar/AddBar.vue";
import VeeValidate from "vee-validate";
import ShowBeer from "./components/Beer/ShowBeer/ShowBeer.vue";
import UpdateBeer from "./components/Beer/UpdateBeer/UpdateBeer.vue";
import AddBeer from "./components/Beer/AddBeer/AddBeer.vue";
import ListBars from "./components/Bar/ListBars/ListBars.vue";
import Home from "../src/components/Home/Home.vue";

import BootstrapVue from "bootstrap-vue/dist/bootstrap-vue.esm";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

Vue.use(BootstrapVue);

Vue.use(VueRouter);
Vue.use(VeeValidate);

const routes = [
  { path: "/bars", component: ListBars },
  { path: "/bar/:id", component: ShowBar },
  { path: "/bar/:id/update", component: UpdateBar },
  { path: "/beers", component: ListBeers },
  { path: "/add_bar", component: AddBar },
  { path: "/beers", component: ListBeers },
  { path: "/beer/:id", component: ShowBeer },
  { path: "/beer/:id/update", component: UpdateBeer },
  { path: "/add_beer", component: AddBeer },
  { path: "/", component: Home }
];

const router = new VueRouter({
  routes: routes
});

new Vue({
  el: "#app",
  router: router,
  render: h => h(App)
});
